import React from 'react';
import './App.css';
import { Link,  Route, Switch, } from "react-router-dom"
import Home from "./components/Paginas/Home"
import Galeria from "./components/Paginas/Galeria"
import Contacto from "./components/Paginas/Contacto"
import QuienSoy from "./components/Paginas/QuienSoy"

import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import { BrowserRouter as Router } from "react-router-dom"

function App() {
  
  return (
    <div className="App">
    <Router>
        <Switch>
          <Route
            path="/"
            exact
            render={props => <Home/>}
          />
          <Route
            path="/galeria"
            exact
            render={props => <Galeria/>}
          />
           <Route
            path="/contacto"
            exact
            render={props => <Contacto/>}
          />
            <Route
            path="/quien"
            exact
            render={props => <QuienSoy/>}
          />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
