import React, {Component} from 'react';
import LayoutMain from '../Layouts/LayoutMain'
import CardJumbo4 from "../Layouts/CardJumbo4"


export default class QuienSoy extends Component {
    render() {
        return (
            <LayoutMain>
            <CardJumbo4/>
            <div className="quienSoy col-md-12">
            <p style={{fontFamily: "Tw Cen MT", fontSize:'25px', color:'white', textAlign:'center'}}> 
              Soy un egresado de la UANL generación 1985-1991 como arquitecto. Integrante de una familia de 9 hermanos dedicados a la construcción, desde joven tuve la inquietud de seguir por el arte de el dibujo y la pintura teniendo logros en esta última. 
              Tuve la oportunidad de estar en una preparatoria técnica enfocada a la construcción y la cual me dio las bases para estudiar arquitectura, ejerciendo la carrera durante más de 30 años.
              Después de haber tenido conocimientos de arquitectura decidí ofrecer mis servicios de forma profesional al ramo de la casa habitación mejorando los diseños hasta hoy existentes.
            </p> 
            </div>
            </LayoutMain>
        )
    }
}

