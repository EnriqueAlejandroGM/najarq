import React, {Component} from 'react';
import LayoutMain from '../Layouts/LayoutMain'
import CardJumbo3 from "../Layouts/CardJumbo3"
import { FaPhone } from "react-icons/fa";
import { GoMail } from "react-icons/go";
import { FaFacebookSquare } from "react-icons/fa";

export default class Home extends Component {
    render() {
        return (
            <LayoutMain>
            <CardJumbo3/>
            <div className='row' style={{color: "white"}}>
              <div className='col-md-6'  style={{padding:'2rem', }}>
                <h1 style={{fontFamily: "Tw Cen MT Condensed"}}>Puedes contactarme por:</h1>
                <p style={{fontSize:'2rem', fontFamily: "Tw Cen MT"}}><FaPhone/> 81 2199 2488</p>
                <p style={{fontSize:'2rem',fontFamily: "Tw Cen MT" }}><GoMail/> isidronajerarq@gmail.com</p>
              </div>
              <div className='col-md-5' style={{padding:'2rem', }}>
                <h1 style={{fontFamily: "Tw Cen MT Condensed"}}>Redes Sociales</h1>
                <a target="_blank" href="https://www.facebook.com/Arq-Isidro-N%C3%A1jera-103836737847000/?epa=SEARCH_BOX"><p style={{fontSize:'2rem', color:"white", fontFamily: "Tw Cen MT"}}><FaFacebookSquare/> Arq Isidro Nájera</p></a>
              </div>
            </div>
            </LayoutMain>
        )
    }
}

