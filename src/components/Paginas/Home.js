import React, {Component} from 'react';
import LayoutMain from '../Layouts/LayoutMain'
import Carrusel from '../Layouts/Carrusel'
import CardJumbo from "../Layouts/CardJumbo"


export default class Home extends Component {
    render() {
        return (
            <LayoutMain>
            <CardJumbo/>
            <Carrusel/>
            </LayoutMain>
        )
    }
}

