import React, { Component, useState, useCallback } from 'react'
import LayoutMain from '../Layouts/LayoutMain'
import CardJumbo2 from '../Layouts/CardJumbo2'
import { render } from "react-dom";
import Carousel, { Modal, ModalGateway } from "react-images";
import Gallery from 'react-photo-gallery'
import imagen1 from './../../img/casas/casaAllende.jpg'
import imagen2 from './../../img/casas/casaCarolcoGranada.jpg'
import imagen3 from './../../img/casas/casaBrisas.jpg'
import imagen4 from './../../img/casas/casaVista.jpg'
import imagen5 from './../../img/casas/casaValle.jpg'
import imagen6 from './../../img/casas/casaSanta.jpg'
import imagen7 from './../../img/casas/casaPaseo.jpg'
import imagen8 from './../../img/casas/casaLoma.jpg'
import imagen9 from './../../img/casas/casaJoyaAbedul.jpg'
import imagen10 from './../../img/casas/casaJoya.jpg'
import imagen11 from './../../img/casas/casaCarolcoRotonda.jpg'
import imagen12 from './../../img/casas/casaBellevile.jpg'
import imagen13 from './../../img/casas/casaAlamo/casaAlamo1.jpg'
import imagen14 from './../../img/casas/casaAlamo/casaAlamo2.jpg'
import imagen15 from './../../img/casas/casaCarolco/casaCarolcoSector1.jpg'
import imagen16 from './../../img/casas/casaCarolco/casaCarolcoSectore2.jpg'
import imagen17 from './../../img/casas/casaCumbres/casaCumbres1.png'
import imagen18 from './../../img/casas/casaCumbres/casaCumbres2.jpg'
import imagen19 from './../../img/casas/casaCumbres/casaCumbres3.jpg'
import imagen20 from './../../img/casas/casaSierraAlta/casaSierra1.jpg'
import imagen21 from './../../img/casas/casaSierraAlta/casaSierra2.jpg'

const photos = [
  {
    src: imagen1,
    width: 5,
    height: 3
  },
  {
    src: imagen2,
    width: 4,
    height: 3
  },
   {
    src: imagen3,
    width: 4,
    height: 3
  }, {
    src: imagen4,
    width: 5,
    height: 3
  }, {
    src: imagen5,
    width: 4,
    height: 3
  }, {
    src: imagen6,
    width: 5,
    height: 3
  }, {
    src: imagen7,
    width: 5,
    height: 3
  },{
    src: imagen8,
    width: 4,
    height: 3
  },{
    src: imagen9,
    width: 4,
    height: 3
  },{
    src: imagen10,
    width: 5,
    height: 3
  },{
    src: imagen11,
    width: 5,
    height: 3
  },{
    src: imagen12,
    width: 4,
    height: 3
  },{
    src: imagen13,
    width: 4,
    height: 3
  },{
    src: imagen14,
    width: 5,
    height: 3
  },{
    src: imagen15,
    width: 5,
    height: 3
  },{
    src: imagen16,
    width: 5,
    height: 3
  },{
    src: imagen17,
    width: 4,
    height: 3
  },{
    src: imagen18,
    width: 5,
    height: 3
  },{
    src: imagen19,
    width: 4,
    height: 3
  },{
    src: imagen20,
    width: 4,
    height: 3
  },{
    src: imagen21,
    width: 5,
    height: 3
  },
];

var Aboutme = function (){
        const [currentImage, setCurrentImage] = useState(0);
        const [viewerIsOpen, setViewerIsOpen] = useState(false);

        const openLightbox = useCallback((event, { photo, index }) => {
            setCurrentImage(index);
            setViewerIsOpen(true);
        }, []);

        const closeLightbox = () => {
            setCurrentImage(0);
            setViewerIsOpen(false);
        }
    return (
          <div>
            <LayoutMain>
                <CardJumbo2/>
                <Gallery photos={photos} onClick={openLightbox} className='galeria'/>
                    <ModalGateway>
                        {viewerIsOpen ? (
                        <Modal onClose={closeLightbox}>
                            <Carousel
                            currentIndex={currentImage}
                            views={photos.map(x => ({
                                ...x,
                                srcset: x.srcSet,
                                caption: ""
                            }))}
                            />
                        </Modal>
                        ) : null}
                    </ModalGateway>
                <div style={{margin: "20px"}}>
                </div>
            </LayoutMain>
            </div>
        )
}
export default Aboutme