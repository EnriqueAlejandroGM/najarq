import React, { Component } from 'react';
import { Link } from "react-router-dom"
import logo from './../../img/logoo.png'


class Navegation extends Component {
   render() {
      return (
         <nav className="navbar navbar-expand navbar-dark" style={{backgroundColor: "rgb(0,0,0,0.5)", position: "fixed", width: "100%", margin: "0", padding: "0", zIndex: "950", overflow: "hidden"}}>
         <div className="container">
         <a className="navbar-brand"><Link to="/"><img width="140px" height="50px" src={logo} /></Link></a>
            <div className="collapse navbar-collapse">
         <ul className="navbar-nav ml-auto">
         <li className="nav-item"><a><Link to="/galeria"  className="nav-link">Galería</Link></a></li>
         <li className="nav-item"><a><Link to="/contacto"  className="nav-link">Contacto</Link></a></li>
         <li className="nav-item"><a><Link to="/quien"  className="nav-link">¿Quién soy?</Link></a></li>
         </ul>
            </div>
         </div>  
         </nav>
      );
   }
}

export default Navegation;