import React from "react";
import { Button } from 'reactstrap';
import { UncontrolledCarousel } from 'reactstrap';
import { Link } from "react-router-dom"
import imagen1 from './../../img/casas/casaAllende.jpg'
import imagen2 from './../../img/casas/casaBrisas.jpg'
import imagen3 from './../../img/casas/casaPaseo.jpg'

class Carrusel extends React.Component {
  render() { 
    const items = [
      {
        src:
          imagen1,
        altText: 'casaAllende'
      },
      {
        src:
          imagen2,
        altText: 'casaBrisas'
      },
      {
        src:
          imagen3,
        altText: 'casaPaseo'
      }
    ];
    return (
      <div>
        <UncontrolledCarousel items={items} />
        <div style={{margin: "20px"}}>
         <Link to='/galeria'><Button outline color="secondary" size="lg">Ir a la galería completa</Button></Link>
        </div>
      </div>
    );
  }
}
export default Carrusel;
