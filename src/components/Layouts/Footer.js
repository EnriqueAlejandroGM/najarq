import React, { Component } from 'react'
import { Link } from "react-router-dom"
import logo from './../../img/logoo.png'
import { FaFacebookSquare } from "react-icons/fa";


export default class Piedepagina extends Component {
    render() {
        return (
            <footer className="page-footer font-small" style={{backgroundColor: "black"}}>
            <div className="container">
                <div className="row">
                    <div className="col-md-6 py-5" style={{textAlign:"left"}}>
                        <a><Link to="/"><img width="140px" height="50px" src={logo} /></Link></a>
                        <ul className="navbar-nav" style={{color:"black"}}>
                            <li><a style={{color: "#757575"}} href="/galeria" className="nav-link" >Galería</a></li>
                            <li><a style={{color: "#757575", hover:"white"}} href="/contacto" className="nav-link">Contacto</a></li>
                        </ul>
                    </div>
                    <div className="col-md-6 py-5"  style={{textAlign:"right"}}>
                        <h5  style={{color: '#9e9e9e'}}>Redes Sociales</h5>
                        <a target="_blank" href="https://www.facebook.com/Arq-Isidro-N%C3%A1jera-103836737847000/?epa=SEARCH_BOX"><FaFacebookSquare style={{color: 'white'}} size={28}/></a>
                    </div>
                </div>
            </div>
            </footer>
        )
    }
}