import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import portada from './../../img/portada.jpg'

const CardJumbo = (props) => {
  return (
    <div className="jumbo">
      <Jumbotron className='rounded-0' style={{color: "#e0e0e0", backgroundImage: `url(${portada})`, marginTop:"5px"}}>
        <Container fluid={true}>
          <h1 className="display-3" style={{fontFamily: "Tw Cen MT Condensed"}}>¿Quién soy?</h1>
        </Container>
      </Jumbotron>
    </div>
  );
};

export default CardJumbo;
