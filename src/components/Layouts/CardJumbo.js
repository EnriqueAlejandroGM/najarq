import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import portada from './../../img/portada.jpg'

const CardJumbo = (props) => {
  return (
    <div className="row">
      <div className="col-md-6 col-sm-12 jumbo">
        <Jumbotron className='rounded-0' style={{color: "#e0e0e0", background: "rgb(0,0,0)", marginTop:"50px"}}>
          <Container fluid={true}>
            <h1 className="display-3" style={{fontFamily: "Tw Cen MT Condensed", fontSize:"100px"}}>Arq. Isidro Nájera Puente</h1>
          </Container>
        </Jumbotron>
      </div>
            <div className="col-md-6 col-sm-12">
            <p className="quien" style={{fontFamily: "Tw Cen MT", fontSize:'19px', color:'white', textAlign:'center', marginTop:"200px"}}> 
              Soy un egresado de la UANL generación 1985-1991 como arquitecto. Integrante de una familia de 9 hermanos dedicados a la construcción, desde joven tuve la inquietud de seguir por el arte de el dibujo y la pintura teniendo logros en esta última. 
              Tuve la oportunidad de estar en una preparatoria técnica enfocada a la construcción y la cual me dio las bases para estudiar arquitectura, ejerciendo la carrera durante más de 30 años.
              Después de haber tenido conocimientos de arquitectura decidí ofrecer mis servicios de forma profesional al ramo de la casa habitación mejorando los diseños hasta hoy existentes.
            </p> 
            </div>
    </div>
  );
};

export default CardJumbo;
